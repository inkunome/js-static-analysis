// import { reachabilityAnalysis } from "./flow-analysis";
import { load } from "./load";

async function main() {
  const metamodel = await load("class.js");

  console.log(metamodel.main().instructions);

  // console.log(reachabilityAnalysis(metamodel));
}

main();
